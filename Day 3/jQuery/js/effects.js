$(document).ready(function() {
    // Show image
    $('#show-image').on('click', function() {
        $('.centered-image').show();
    });

    // Hide image
    $('#hide-image').on('click', function() {
        $('.centered-image').hide();
    });

    // Round Corners
    $('#round-corners').on('click', function() {
        $('.centered-image').addClass('rounded');
    });

    // Straight Corners
    $('#straight-corners').on('click', function() {
        $('.centered-image').removeClass('rounded');
    });

    // Add Red Border
    $('#add-red-border').on('click', function() {
        $('.centered-image').addClass('red-border');
    });

    // Remove Red Border
    $('#remove-red-border').on('click', function() {
        $('.centered-image').removeClass('red-border');
    });

    // Move UP
    $('#move-up').on('click', function() {
        $('.centered-image').animate({
            top: "-=10px",
        }, 1000);
    });

    // Move Down
    $('#move-down').on('click', function() {
        $('.centered-image').animate({
            top: "+=10px",
        }, 1000);
    });
});
