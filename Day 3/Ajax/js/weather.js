/*
For temperature in Fahrenheit use units = imperial
For temperature in Celsius use units = metric
Temperature in Kelvin is used by default, no need to use units parameter in API call
*/

var cityId = 725993; // Veliko Turnovo
var api = '401e6589b3e81bf8b63f906e28fd698c';
var units = 'metric';

$(document).ready(function() {
    var root = 'http://api.openweathermap.org/data/2.5/weather';

    $.ajax({
        url: root + '?id=' + cityId + '&appid=' + api + '&units=' + units,
        method: 'GET'
    }).then(function(data) {
        $('#city-name').text('Weather in ' + data.name + ', ' + data.sys.country);

        var table = $("#weather-table tbody");
        table.append(
            "<tr>" +
            "<td>Weather</td>" +
            "<td>" + data.weather[0].main + "</td>" +
            "</tr>" +

            "<tr>" +
            "<td>Condition</td>" +
            "<td>" + data.weather[0].description + "</td>" +
            "</tr>" +

            "<tr>" +
            "<td>Wind</td>" +
            "<td>" + data.wind.speed + " m/s</td>" +
            "</tr>" +

            "<tr>" +
            "<td>Cloudiness</td>" +
            "<td>" + data.clouds.all + " %</td>" +
            "</tr>" +

            "<tr>" +
            "<td>Pressure</td>" +
            "<td>" + data.main.pressure + " hpa</td>" +
            "</tr>" +

            "<tr>" +
            "<td>Humidity</td>" +
            "<td>" + data.main.humidity + " %</td>" +
            "</tr>" +

            "<tr>" +
            "<td>Temperature</td>" +
            "<td>" + data.main.temp + " &#8451;</td>" +
            "</tr>" +

            "<tr>" +
            "<td>Max Temperature</td>" +
            "<td>" + data.main.temp_max + " &#8451;</td>" +
            "</tr>" +

            "<tr>" +
            "<td>Sunrise</td>" +
            "<td>" + new Date(data.sys.sunrise * 1000).toLocaleTimeString() + "</td>" +
            "</tr>" +

            "<tr>" +
            "<td>Sunset</td>" +
            "<td>" + new Date(data.sys.sunset * 1000).toLocaleTimeString() + "</td>" +
            "</tr>");
    });
});
