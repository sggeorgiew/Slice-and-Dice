$(document).ready(function() {
    chart1();
    chart2();
});

var chart1 = function() {
    var ctx = $('#chart-area');
    window.myPie = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    27,
                    33,
                    10,
                    40,
                    60,
                ],
                backgroundColor: [
                    window.chartColors.red,
                    window.chartColors.orange,
                    window.chartColors.yellow,
                    window.chartColors.green,
                    window.chartColors.blue,
                ],
                label: 'Dataset 1'
            }],
            labels: [
                "Red",
                "Orange",
                "Yellow",
                "Green",
                "Blue"
            ]
        },
        options: {
            responsive: true
        }
    });
}

var chart2 = function() {
    new Chartist.Bar('#chart2', {
        labels: ['Q1', 'Q2', 'Q3', 'Q4'],
        series: [
            [800000, 1200000, 1400000, 1300000],
            [200000, 400000, 500000, 300000],
            [100000, 200000, 400000, 600000]
        ]
    }, {
        stackBars: true,
        axisY: {
            labelInterpolationFnc: function(value) {
                return (value / 1000) + 'k';
            }
        }
    }).on('draw', function(data) {
        if (data.type === 'bar') {
            data.element.attr({
                style: 'stroke-width: 50px'
            });
        }
    });
}
