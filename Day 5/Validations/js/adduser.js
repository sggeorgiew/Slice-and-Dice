function addUser() {
    if ($('#add-user-form').valid()) {
        $.ajax('http://jsonplaceholder.typicode.com/users', {
            method: 'POST',
            data: $('#add-user-form').serialize()
        }).then(function(data) {
            new PNotify({
                title: 'Success!',
                text: 'The user was successfully added to our database.',
                type: 'success'
            });
        });
    } else {
        new PNotify({
            title: 'Failed!',
            text: 'Please fill all required fields and try again.',
            type: 'error'
        });
    }
}
