$(document).ready(function() {
    var root = 'http://jsonplaceholder.typicode.com';

    $('#users-table').DataTable({
        "ajax": function(data, callback) {
            $.ajax({
                'type': 'GET',
                'url': root + '/users',
                'cache': false,
                'contentType': 'application/json;',
                'dataType': 'json',
                'data': data,
                'success': function(jSon) {
                    callback({
                        data: jSon
                    });
                },
                'error': function() {
                    callback([]);
                }
            });
        },
        "columns": [{
                'data': 'id'
            },
            {
                'data': 'name'
            },
            {
                'data': 'username'
            },
            {
                'data': 'email'
            },
            {
                'data': 'address.city'
            },
            {
                'data': 'phone'
            },
            {
                'data': 'website'
            },
            {
                'data': 'company.name'
            }
        ]
    });
});
